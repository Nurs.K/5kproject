from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include
from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.views import TokenRefreshView

from .views import api_root

schema_view = get_schema_view(
    openapi.Info(
        title="50K Project CRM API",
        default_version="v1",
        description="API for CRM OF 50K Project",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="nursultandev@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

api_patterns = [
    path("", api_root, name="api-root"),
    path("users/", include("apps.users.urls")),
    path("courses/", include("apps.courses.urls")),
    path("lessons/", include("apps.lessons.urls")),
    path("homework/", include("apps.homeworks.urls")),
    path("schedules/", include("apps.schedules.urls")),
    path('examinations/', include('apps.exams.urls')),
    path('analytics/', include('apps.analytics.urls')),
]

urlpatterns = [
    path("jet/", include("jet.urls", "jet")),
    path("admin/", admin.site.urls),
    path("api/", include(api_patterns)),
    path("auth/", include("rest_framework.urls")),
    # auth
    path("api/token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("api/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    # documentation
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path(
        "redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc-ui"
    ),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
