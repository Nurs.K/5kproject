from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


@api_view(["GET"])
def api_root(request, format=None):
    response = Response(
        {
            "trainers": reverse("trainers-list", request=request, format=format),
            "students": reverse("students-list", request=request, format=format),
            "export_students": reverse('export_students-list', request=request, format=format),
            "admins": reverse("administrators-list", request=request, format=format),
            "courses": reverse("courses-list", request=request, format=format),
            "lessons": reverse("lessons-list", request=request, format=format),
            "homeworks": reverse("homeworks_obj-list", request=request, format=format),
            "schedules": reverse("schedules-list", request=request, format=format),
            "examinations": reverse("exams-list", request=request, format=format),
        }
    )
    return response
