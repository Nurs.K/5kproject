from django.db import models
from apps.homeworks import choices
from apps.courses.models import Course
from apps.students.models import Student
from apps.trainers.models import Trainer
from django.db.models.signals import post_save
from django.dispatch import receiver
from utils import generators

from crm.storage import PublicMediaStorage


class Homework(models.Model):
    course = models.ForeignKey(
        Course,
        on_delete=models.SET_NULL,
        verbose_name='Курс',
        related_name='homeworks',
        null=True
    )
    title = models.CharField(
        max_length=255,
        verbose_name='Название',
        db_index=True
    )
    description = models.TextField(
        verbose_name='Инструкции (необязательно)',
        blank=True, null=True,
    )
    file = models.FileField(
        verbose_name='Инструкция в файле',
        upload_to=generators.generate_document_filename,
        storage=PublicMediaStorage(),
        blank=True, null=True
    )
    deadline_date = models.DateField(
        verbose_name='Срок сдачи (Дата)',
    )
    deadline_time = models.TimeField(
        verbose_name='Срок сдачи (Время (необязательно))',
        blank=True, null=True
    )
    points = models.PositiveIntegerField(
        verbose_name='Баллы'
    )

    def __str__(self):
        return f"{self.title}"


class StudentAnswer(models.Model):
    homework = models.ForeignKey(
        Homework,
        on_delete=models.CASCADE,
        related_name='student_answer',
    )
    student = models.ForeignKey(
        Student,
        on_delete=models.CASCADE,
        verbose_name='Студетн',
        related_name='student_user',
    )
    text_answer = models.TextField(
        verbose_name='Ответ студента в текстовом формате',
        blank=True, null=True
    )
    file_answer = models.FileField(
        verbose_name='Ответ студента в файловом формате',
        upload_to=generators.generate_document_filename,
        storage=PublicMediaStorage(),
        blank=True, null=True
    )
    status_answer = models.CharField(
        verbose_name='Статус',
        max_length=255,
        choices=choices.STUDENT_ANSWER_STATUS,
        default=choices.STUDENT_ANSWER_STATUS[1][1],
        blank=True, null=True,
    )

    def __str__(self):
        return f"{self.homework.title} -- {self.student.user.email}"


class CheckHomework(models.Model):
    trainer = models.ForeignKey(
        Trainer,
        on_delete=models.CASCADE,
        blank=True, null=True,
        related_name='check_homework'
    )
    text = models.TextField(
        verbose_name='Комментарии',
        blank=True, null=True
    )
    grade = models.PositiveIntegerField(
        verbose_name='Оценка',
        default=0,
    )
    time_send = models.DateTimeField(
        verbose_name="Дата отправки комментарии",
        auto_now_add=True,
    )
    check_answer = models.ForeignKey(
        StudentAnswer,
        on_delete=models.CASCADE,
        blank=True, null=True,
        related_name='student_check'
    )

    def __str__(self):
        return f"{self.trainer.user.email} -- {self.check_answer.student.user.email}"


@receiver(post_save, sender=Homework)
def create_student_answer(sender, instance, created, *args, **kwargs):
    if created:
        students = instance.course.students.all()
        for student in students:
            StudentAnswer.objects.create(homework=instance, student=student)
