# Generated by Django 3.2 on 2021-05-19 19:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homeworks', '0003_auto_20210519_1941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='checkhomework',
            name='time_send',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Дата отправки комментарии'),
        ),
    ]
