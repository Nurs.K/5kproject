from django.db.models import Count, F
from django.db.models.functions import ExtractYear
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.analytics.serializers import (
    StudentsStatusesSerializer,
    StudentsStatusesYearsSerializer,
    StudentsCoursesSerializer,
    StudentsCoursesYearSerializer,
    StudentsActiveCoursesSerializer,
    StudentsEmployedSerializer,
)
from apps.courses.models import Course
from apps.students.choices import STUDENT_STATUS_CHOICES
from apps.students.models import Student


def response_example(serializer, description=''):
    return openapi.Response(description, serializer)


class StudentsAnalyticsViewSet(viewsets.ViewSet):
    @swagger_auto_schema(
        responses={
            200: response_example(
                StudentsStatusesSerializer,
                description='Кол-во студентов за ВСЁ ВРЕМЯ со статусами'
            )
        }
    )
    @action(detail=False, url_path='students-statuses-all')
    def students_statuses_all(self, request):
        students = Student.objects.values('status'). \
            annotate(count=Count('status')). \
            order_by('status')
        return Response(students)

    @swagger_auto_schema(
        responses={
            200: response_example(
                StudentsStatusesYearsSerializer,
                description='Кол-во студентов по годам со статусами'
            )
        }
    )
    @action(detail=False, url_path='students-statuses-years')
    def students_statuses_years(self, request):
        active = Student.objects.filter(status=STUDENT_STATUS_CHOICES[0][0]). \
            annotate(year=ExtractYear('enrollment_date')). \
            values('year').annotate(count=Count('id')).order_by('year')

        graduated = Student.objects.filter(status=STUDENT_STATUS_CHOICES[2][0]). \
            annotate(year=ExtractYear('finish_date')). \
            values('year').annotate(count=Count('id')).order_by('year')

        left = Student.objects.filter(status=STUDENT_STATUS_CHOICES[1][0]). \
            annotate(year=ExtractYear('finish_date')). \
            values('year').annotate(count=Count('id')).order_by('year')
        data = {
            'active': active,
            'graduated': graduated,
            'left': left
        }
        return Response(data)

    @swagger_auto_schema(
        responses={
            200: response_example(
                StudentsCoursesSerializer,
                description='Кол-во студентов за всё время по направлениям'
            )
        }
    )
    @action(detail=False, url_path='students-courses-all')
    def students_courses_all(self, request):
        courses = Course.objects.annotate(count=Count('students')). \
            values('count', 'title')
        return Response(courses)

    @swagger_auto_schema(
        responses={
            200: response_example(
                StudentsCoursesYearSerializer,
                description='Кол-во студентов по годам и направлениям'
            )
        }
    )
    @action(detail=False, url_path='students-courses-years')
    def students_courses_years(self, request):
        courses = Course.objects.annotate(count=Count('students')). \
            annotate(year=ExtractYear('students__enrollment_date')). \
            values('count', 'title', 'year')
        return Response(courses)

    @swagger_auto_schema(
        responses={
            200: response_example(
                StudentsActiveCoursesSerializer,
                description='Кол-во активных студентов по направлениям'
            )
        }
    )
    @action(detail=False, url_path='students-amount-years')
    def students_amount_years(self, request):
        students = Student.objects.annotate(year=ExtractYear('enrollment_date')). \
            values('year').annotate(count=Count('id')).order_by('year')
        return Response(students)

    @swagger_auto_schema(
        responses={200: response_example(
            StudentsEmployedSerializer,
            description='Кол-во трудоустроенных за всё время и по годам'
        )}
    )
    @action(detail=False, url_path='students-employed')
    def students_employed(self, request):
        employed_count = Student.objects.filter(place_of_work__isnull=False).count()
        employed = Student.objects.filter(place_of_work__isnull=False). \
            annotate(year=ExtractYear('enrollment_date')). \
            values('year').annotate(count=Count('id')).order_by('year')
        data = {
            'employed_count': employed_count,
            'employed': employed
        }
        return Response(data)

    @swagger_auto_schema(
        responses={200: response_example(
            StudentsCoursesSerializer,
            description='Кол-во студентов по регионам'
        )}
    )
    @action(detail=False, url_path='students-active-regions')
    def students_active_regions(self, request):
        students = Student.objects.annotate(title=F('region')). \
            values('title').annotate(gender=Count('region')). \
            order_by('region')
        return Response(students)

    @swagger_auto_schema(
        responses={200: response_example(
            StudentsCoursesSerializer,
            description='пол'
        )}
    )
    @action(detail=False, url_path='students-gender')
    def students_gender(self, request):
        students = Student.objects.annotate(title=F('user__gender')). \
            values('title').annotate(gender=Count('user__gender')). \
            order_by('user__gender')
        return Response(students)
