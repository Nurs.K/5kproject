from rest_framework.routers import DefaultRouter


from apps.analytics import views

router = DefaultRouter()
router.register(r'students', views.StudentsAnalyticsViewSet, basename='students')

urlpatterns = []

urlpatterns += router.urls
