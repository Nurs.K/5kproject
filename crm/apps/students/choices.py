ACTIVE = "Активный"
LEFT = "Ушёл"
GRADUATED = "Окончил"

STUDENT_STATUS_CHOICES = ((ACTIVE, "Активный"), (LEFT, "Ушёл"), (GRADUATED, "Окончил"))

# ====================

BEGINNER = "Beginner"
ELEMENTARY = "Elementary"
INTERMEDIATE = "Intermediate"
ADVANCED = "Advanced"

ENGLISH_LEVEL_CHOICES = (
    (BEGINNER, "Beginner"),
    (ELEMENTARY, "Elementary"),
    (INTERMEDIATE, "Intermediate"),
    (ADVANCED, "Advanced"),
)

# ===================

IK = "IK"
CHUY = "CH"
NARYN = "NR"
TALAS = "TS"
OSH = "OS"
JALAL = "JL"
BATKEN = "BT"

OBLAST_CHOICES = (
    (IK, "Иссык-Кульская область"),
    (CHUY, "Чуйская область"),
    (NARYN, "Нарынская область"),
    (TALAS, "Таласская область"),
    (OSH, "Ошская область"),
    (JALAL, "Джалал-Абадская область"),
    (BATKEN, "Баткенская область"),
)
