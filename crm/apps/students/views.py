from apps.students.models import Student
from apps.students.serializers import StudentDetailSerializer
from apps.students.serializers import StudentSerializer
from apps.students.serializers import ExportStudentData
from drf_renderer_xlsx.mixins import XLSXFileMixin
from drf_renderer_xlsx.renderers import XLSXRenderer
from django.db.models import F
from rest_framework import mixins
from rest_framework import viewsets


class StudentViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    serializer_class = StudentSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Student.objects.annotate(course_name=F("course__title"))
        return Student.objects.all()

    def get_serializer_class(self):
        if self.action == "retrieve":
            return StudentDetailSerializer
        return self.serializer_class


class ExportRenderStudent(XLSXFileMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Student.objects.annotate(course_name=F("course__title"))
    serializer_class = ExportStudentData
    renderer_classes = [XLSXRenderer]
    filename = 'my_export.xlsx'
