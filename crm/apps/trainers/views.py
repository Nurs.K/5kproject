from apps.trainers.models import Trainer
from apps.trainers.serializers import TrainerSerializer
from django.db.models import F
from rest_framework import mixins
from rest_framework import viewsets


class TrainerViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    queryset = Trainer.objects.all()
    serializer_class = TrainerSerializer
