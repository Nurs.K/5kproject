from apps.trainers.models import Trainer
from apps.users.serializers import UserSerializer
from rest_framework import serializers


class TrainerSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Trainer
        fields = "__all__"
