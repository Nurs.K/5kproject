from rest_framework.routers import DefaultRouter
from apps.lessons.views import LessonViewSetAPI

router = DefaultRouter()
router.register('', LessonViewSetAPI, basename='lessons')

urlpatterns = router.urls
