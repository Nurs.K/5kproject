from apps.administrators.models import Administrator
from apps.administrators.serializers import AdministratorSerializer
from django.db.models import F
from rest_framework import mixins
from rest_framework import viewsets


class AdministratorViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    serializer_class = AdministratorSerializer
    queryset = Administrator.objects.all()
